DON'T FORGET TO CHANGE THE NAME OF THE FILE IN THE CMAKELISTS


The main code is in predictable_kuka_v1/src/dmp_ros_ctl_ik.cpp
The code is modeled to work hand in hand with the vrep setup in vrep/dmpAngleTest.ttt

Things that have to be changed/added before working with the code:
1. In the includes the paths for the headers has to be adjusted.


How to run the code? (from the local machine at LSR)
1. In hamburger: Go to Documents/dmpbbo_ws && source devel/setup.bash
2. ssh -X hrc@nalle and run roscore
3. Run vrep: cd Documents/V-Rep.. && ./vrep.sh and choose the dmpAngleTest.ttt scene
5. Open Qualisys on the windows computer, and roslaunch qualisys qualisys.launch
6. Roslaunch promp_prediction ...config:=multiple_targets (if multiple targets are chosen) this code publishes the human duration at the end of each human trajectory
7. Switch on the Kuka robot and run the file in siemens/ros_control
8. ssh -X hrc@nalle and roslaunch hrc_cobot_robot standing.launch
9. rosrun rqt_gui rqt_gui, in the controller namespace choose the desired ros controller (joint_position_controller or joint_impedance_controller)
10. In the terminal of step 1. rosrun dmpbbo_node dmp_ros_ctl_ik


Design parameters that can be changed:
-In dmp_ros_ctl_ik.cpp you can specify the robot starting position as joint angles (move the kuka in gravity compensation mode and copy the angles, for angle 2 you need to decrease 90 degrees and E1 on the kuka is the third angle)
-You can specify the number of goals and their position in cartesian space with respect to the vrep world frame.
-You can modify the lambda (the strength of the potential field) in the dynamicPotentialForce function.
-You can modify the effect of the null-space constraints in the getCurrentVelocities function.
-You can modify the number of samples per goal and the number of updates.
-You can modify the computeCosts function.

plotting:
On hamburger: in Documents/dmpbbo_ws/Results all the files get saved from the program. In this file there are some MATLAB scripts that read those files and plot them.

some problems that can happen and their solutions:
-ROS communication sometimes drops. just exit all terminals and restart roscore.
-If the robot is not responding when running it in the first time, run fridemos/Jtest first.
-If you encounter some segmentation fault errors after changing something they might be due to the serialization of the class and the loading, so check that again. 

vrep:
The dmpAngleTest.ttt scene includes a lot of child scripts that subscribe and publish to different topics (all subscribers and publishers in the code side are defined in the beginning of the main function in dmp_ros_ctl_ik.cpp)
The Cuboid is the recognized obstacle, of which the position gets published and the distance calculations being made.
The mannequin is programmed to just follow the cuboid with its right hand and is only for visualization.
You can program the cuboid to move during simulation, simply uncomment the code in its child script. 
If qualisys and the code are running the cuboid follows the position of wrist_right detected by qualisys
The robot recieves the joint positions published for the impedance controller, if you are using position control change the subscriber in the child script of lwr


Location of vrep scene on hamburger: Documents/V-Rep.../scenes/dmpAngleTest.ttt
Location of main code: Documents/dmpbbo_ws/src/predictable_kuka_v1/src/dmp_ros_ctl_ik.cpp
Location of qualisys workspace: Documents/dmpbbo_ws/src/qualisys
Location of promp code: catkin_ws/src/promp_prediction


If you have any questions contact Mariam at mariam.elsayed@tum.de